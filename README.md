# baylorswimclub.com

This is an ideas doc for now. 

Thoughts: 

*  TypeScript for front-end. Host on Netlify? Seems like a simple service for hosting.
*  Back-end: Could use Netlify functions? Seems to be a AWS Lambda solution. Or maybe host Python + Flask on Heroku? <<-- Heroku cold starts are bad
*  Database: Find some free solution. Honestly kind of sick of AWS and its unnecessary complication considering the size.

Features:
*  Create/store workouts by porting over the code from rafnel.com - all workouts functionality from rafnel.com
*  Have an admin account for Zac/maybe other officers
*  Roster Management Solution:
     - Admins can add/remove swimmers to the roster
     - Swimmers can have paid/not paid dues - check-box
     - Mark swimmer attendance 
     - View swimmer attendance/yardages
*  Practice Management Solution:
     - Create practices, like on rafnel.com
     - Edit practices
     - Manage folders, like on rafnel.com
     - Create a calendar and assign practices to certain days, also makes notes/title certain days on calendar
     - Integrate with roster solution and assign practices to attending swimmers
     - Be able to make notes on a day's practice / on a certain swimmer (their intervals maybe?)
*  Swimmer Interactions
     - Swimmers can view their own attendance, yardages, sets, intervals, notes made by coach
     - Swimmers could potentially view the week's schedule?